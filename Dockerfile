FROM python:3.9-slim-buster

RUN mkdir /app
WORKDIR /app
COPY ./lib /app/lib
COPY ./main.py /app/main.py
COPY ./requirements.txt /app
COPY ./weatherstations-NL.csv /app/weatherstations-NL.csv
RUN pip3 install -r requirements.txt

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
EXPOSE 8000