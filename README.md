# Precipitation API



## Getting started

sudo docker-compose build

sudo docker-compose up

Open http://0.0.0.0:8000/docs#/default/precipitation_precipitation_post

Send POST HTTP request as request body. 

E.g.

{
  "lat": 52.93,
  "long": 4.78,
  "start_date": 20220601,
  "end_date": 20220628,
  "hour": 1
}

# Visualize the locations on a map

Open Weather station in Netherlands.ipynb on Jupyter Notebook. This item is relevant to the first assignment part. 



